# 3/19/2021

### 48923d5ae79bdc6462f732aa40215d4f01399d77

---

## Flaws

### File [src/app/app.component.html](src/app/app.component.html)

* Помилка в слові _perimetr_
* Варто притримуватись стилю, краще візуально сприймається:
  * між атрибутами і значенням не треба пробілів
  ```html
  [disabled]="disabledButton"
  ```
  ```html
  (click)="drawDots($event)"
  ```
  * між сусідніми елементами пуста строка, якщо атрибутів багато то можна переносити їх на новий рядок з відступом
  ```html
  <div>
    <button 
      class="btn" 
      [disabled]="disabledButton"
      (click)="drawPoligon()" 
    >
      Create shape
    </button>

    <p>
      perimetr is {{ calculatePerimeter() }}
    </p>

    <p>
      area is {{ calculateArea() }}
    </p>
  </div>
  ```
* Інтерполяцію функцій, як для `calculateArea` чи `calculatePerimeter`, в html шаблонах краще не робити, занадто затратно для браузера в продуктивності. І при деяких умовах не працює.
  Краще всі обрахунки проводити в контролері, зберігати результат в властивостях і використовувати в html вже готові значення

### File [src/app/app.component.ts](src/app/app.component.ts)

* всюди або треба писати _public_ для методів або не треба і він буде по-замовчуванню. Має бути один формат
* метод `getCoorinates` може бути _private_ або _protected_. Він не використовується крім контролера ніде
* стиль коду. Логічні блоки (константи, виклики методів, присвоєння значень) краще розділяти пустими строками, простіше сприймається, наприклад:
  ```typescript
  const rect = [];
  const perimeter = [];

  this.context.load();
  this.points.push();

  for (let i = 0; i < 5; i++) {
    ...
  }

  return 'some value'; // перед return завжди пуста строка
  ```
* для аргументів функцій варто старатись вказувати тип і уникати, по можливості, _any_. Є тип _void_, коли метод не повертає нічого
  ```typescript
  public drawDots($event: MouseEvent): void {
  ```
* назви змінних, аргументів функцій краще придумати більше очевидні. Мініфіковані варіанти (`e`, `n`) важко відслідковувати. Для назв краще використовувати _camelCase_ - `positionX`
* для `@ViewChild` можна передавати другий агрумент з опціями `{static: true, read: ElementRef}`. Не обов'язково але більш інформативно
* Строка ~42. Передавати `this.canvas` нема сенсу, в `getCoorinates` (до речі, помилка в слові) можна звертатись так само `this.canvas`
* ~16-17 і ~43-44. Не бачу сенсу зберігати їх окремо. Крім `drawDots` більше ніде не використовується , можна просто використовувати локально.
* ~48 `return` не потрібен, ефекту не дає ніякого
* ~28-30 Цикл можна простіше написати без let point:
  ```typescript
  this.context.moveTo(this.points[0].x, this.points[0].y);

  for (let i = 1; i < this.points.length; i++) {
    this.context.lineTo(this.points[i].x, this.points[i].y);
  }
  ```
  Хоча для навчання краще розуміти суть, тому тут ок.
* методи `calculatePerimeter`, `calculateArea` краще викликати зразу в `drawPoligon` в кінці. Зберігати властивості `perimeter`, `area` в контролері і використовувати їх в html 
* ~46 `+` перед змінною таким чином буде конвертувати її в число. Для подальших розрахунків це може бути важливо, в поточному варіанті результат більше не обчислюється, тому може лишатись строкою. І треба поправити тип повернення метода
* ~77 краще не ставити дві операції послідовно. Менше коду але менш читабельно:
  ```typescript
  area = Math.abs((area + lastSide) / 2);
  
  return area; // пуста строка перед return
  ```
* ~66 В функції `calculatePerimeter`, використовувати return для того щоб закінчити виконання функції, якщо вона зайде в if виглядає не дуже гарно, я б використав `this.points.length > 3` та використав якесь стале значення для `generalPerimeter`, або додав якусь логіку для цього `return`, щоб він повертав щось інформативне

# 3/21/2021

### 25db7b7ad355bcf8d436a51adb81749f4ddbbdfb

---

## Flaws

### File [src/app/app.component.ts](src/app/app.component.ts)

* ~10. Для декоратора не треба пустих строк

### File [src/app/app.component.html](src/app/app.component.html)

* ~9-15. Не user-friendly пусті значення показувати, поки не створена фігура. Можна використати [*ngIf-else](https://angular.io/api/common/NgIf) для випадку, коли `generalPerimeter`, `generalArea` не мають значень

## Extra

* Рекомендую познайомитись з пакетом [prettier](https://prettier.io/) для форматування коду
* Можна зробити додаткову кнопку для видалення всіх вершин фігури, щоб можна було створити нову фігуру і не перезавантажувати сторінку
* Якщо було створено ангулярний проект, тоді можна було б спробувати створити структуру проекту
