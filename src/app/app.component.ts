import { AfterViewInit } from '@angular/core';
import { Component, ElementRef, ViewChild } from '@angular/core';
import { Point } from './app.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements AfterViewInit {
  @ViewChild('polygon') canvas: ElementRef;

  public context: any;
  public generalArea: number;
  public generalPerimeter: number;
  public points: Point[] = [];
  public disabledButton = true;
  public isShowedMeasures = false;
  public colorOfPolygon = '#E09DB2';

  public ngAfterViewInit(): void {
    this.context = this.canvas.nativeElement.getContext('2d');
  }

  public drawPolygon(): void {
    if (this.points.length === 0) {
      return;
    }

    this.context.beginPath();
    this.context.moveTo(this.points[0].x, this.points[0].y);

    for (let i = 1; i < this.points.length; i++) {
      this.context.lineTo(this.points[i].x, this.points[i].y);
    }

    this.context.strokeStyle = this.colorOfPolygon;
    this.context.fillStyle = this.colorOfPolygon;

    this.context.closePath();
    this.context.stroke();
    this.context.fill();

    this.generalArea = this.calculateArea();
    this.generalPerimeter = this.calculatePerimeter();
    this.isShowedMeasures = true;
  }

  public drawDots($event: MouseEvent): void {
    const position = this.getCoordinates($event);
    this.context.fillStyle = this.colorOfPolygon;

    const positionX = position.x;
    const positionY = position.y;

    this.points.push(position);
    this.context.fillRect(positionX - 2, positionY - 2, 4, 4);

    if (this.points.length > 2) {
      this.disabledButton = false;
    }
  }

  private getCoordinates(event): Point {
    const rect = this.canvas.nativeElement.getBoundingClientRect();

    return { x: event.clientX - rect.left, y: event.clientY - rect.top };
  }

  public calculatePerimeter(): number {
    if (this.points.length < 3) {
      return;
    }

    let perimeter = 0;
    const lengthOfArrayPoints = this.points.length - 1;
    const lastSide = Math.sqrt(
      (this.points[lengthOfArrayPoints].x - this.points[0].x) ** 2 +
        (this.points[lengthOfArrayPoints].y - this.points[0].y) ** 2
    );

    for (let i = 0; i + 1 < this.points.length; i++) {
      perimeter =
        perimeter +
        Math.sqrt(
          (this.points[i + 1].x - this.points[i].x) ** 2 +
            (this.points[i + 1].y - this.points[i].y) ** 2
        );
    }

    perimeter = perimeter + lastSide;

    return Number(perimeter.toFixed(2));
  }

  public calculateArea(): number {
    if (this.points.length < 3) {
      return 0;
    }

    let area = 0;
    const lengthOfArrayPoints = this.points.length - 1;
    const lastSide =
      (this.points[0].x + this.points[lengthOfArrayPoints].x) *
      (this.points[0].y - this.points[lengthOfArrayPoints].y);

    for (let i = 0; i + 1 < this.points.length; i++) {
      area =
        area +
        (this.points[i + 1].x + this.points[i].x) *
          (this.points[i + 1].y - this.points[i].y);
    }

    area = Math.abs((area + lastSide) / 2);

    return area;
  }

  public createNewPolygon(): void {
    this.context.clearRect(
      0,
      0,
      this.canvas.nativeElement.width,
      this.canvas.nativeElement.height
    );

    this.points = [];
    this.generalPerimeter = 0;
    this.generalArea = 0;
    this.disabledButton = true;
    this.isShowedMeasures = false;
  }
}
